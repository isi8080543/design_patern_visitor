public class Client {
    public static void main(String[] args) {
        // creation des patient
        RegularPatient regularPatient = new RegularPatient();
        VIPPatient vipPatient = new VIPPatient();

        // creer un visitor pour l'envoie de newslatter
        NewsletterVisitor emailNewsletterVisitor = new EmailNewsletterVisitor();

        // envoie d'email
        regularPatient.accept(emailNewsletterVisitor);
        vipPatient.accept(emailNewsletterVisitor);

    }
}