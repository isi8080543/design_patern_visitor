// Classe pour les patients VIP
class VIPPatient implements Patient {
    @Override
    public void accept(NewsletterVisitor visitor) {
        visitor.visit(this);
    }
}