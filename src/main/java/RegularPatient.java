// Classe pour les patients regulier
class RegularPatient implements Patient {
    @Override
    public void accept(NewsletterVisitor visitor) {
        visitor.visit(this);
    }
}