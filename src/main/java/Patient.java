// Interface pour les patients
interface Patient {
    void accept(NewsletterVisitor visitor);
}