//Interface pour les visiteurs d'envoi de newsletters
interface NewsletterVisitor {
    void visit(RegularPatient patient);
    void visit(VIPPatient patient);
}