// Implémentations concrètes des visiteurs d'envoi de newsletters
class EmailNewsletterVisitor implements NewsletterVisitor {
    @Override
    public void visit(RegularPatient patient) {
        // Envoyer la newsletter par e-mail aux patients réguliers
        System.out.println("Envoie de newsletter aux patients regulier par email.");
    }

    @Override
    public void visit(VIPPatient patient) {
        // Envoyer la newsletter par e-mail aux patients VIP
        System.out.println("Envoie de newsletter aux patients VIP par email.");
    }
}